Rails.application.routes.draw do
  resources :routes, except: [:edit, :update, :destroy]

  get 'stats', to: 'routes#stats'

  root 'routes#new'
end
