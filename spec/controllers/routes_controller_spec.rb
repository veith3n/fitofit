require 'rails_helper'

RSpec.describe RoutesController, type: :controller do
  context '#index' do
    before do
      Route.skip_callback(:validation, :after, :calculate_distance, raise: false)
    end
    it 'returns HTTP success' do
      get :index, params: {}

      expect(response).to have_http_status(:success)
    end

    it 'returns all created Routes' do
      Route.create(starting_point: 'Marszalkowska 6, Warszawa', end_point: 'Nowogrodzka 40, Warszawa')
      Route.create(starting_point: 'Nowolipki 30, Warszawa', end_point: 'Nowogrodzka 40, Warszawa')
      Route.create(starting_point: 'Zelazna 5, Warszawa', end_point: 'Nowogrodzka 40, Warszawa')

      get :index, params: {}

      expect(response).to have_http_status(:success)
      expect(assigns(:routes)).to match_array(Route.all)
    end
  end

  context '#show' do
    before do
      Route.skip_callback(:validation, :after, :calculate_distance, raise: false)
    end
    it 'returns HTTP success' do
      route = Route.create(starting_point: 'Marszalkowska 6, Warszawa', end_point: 'Nowogrodzka 40, Warszawa')

      get :show, params: {id: route.id}

      expect(response).to have_http_status(:success)
    end
  end

  context '#new' do
    it 'returns HTTP success' do
      get :new

      expect(response).to have_http_status(:success)
    end
  end

  context '#create' do
    it 'create route with valid params' do
      params = {starting_point: 'Wiejska 6, Warszawa', end_point: 'Nowy Swiat 6, Warszawa'}

      post :create, params: {route: params}

      expect(Route.last.starting_point).to eq(params[:starting_point])
      expect(Route.last.end_point).to eq(params[:end_point])
    end

    it 'does not create route with invalid params' do
      params = {starting_point: 'Wiejska 6, Warszawa', end_point: nil}

      expect {post :create, params: {route: params}}.to_not change {Route.count}
      expect(subject).to render_template(:new)
    end
  end
end
