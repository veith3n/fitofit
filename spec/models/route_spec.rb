require 'rails_helper'

RSpec.describe Route, type: :model do
  before do
    Route.set_callback(:validation, :after, :calculate_distance, raise: false)
  end
  it 'is valid with valid attributes' do
    expect(Route.new(starting_point: 'Address 1', end_point: 'Address 2')).to be_valid
  end

  it 'calculates distance between starting and end point' do
    route = Route.create(starting_point: 'Zelazna 5, Warszawa', end_point: 'Nowogrodzka 40, Warszawa')
    expect(route.distance.to_f).to eq(1.02)
  end

  context '#scopes' do
    before do
      Route.skip_callback(:validation, :after, :set_date, raise: false)
      Route.skip_callback(:validation, :after, :calculate_distance, raise: false)
    end

    let!(:last_year_route) do
      Route.create(starting_point: 'Miodowa 5', end_point: 'Nowogrodzka 40', date: 1.year.ago)
    end

    let!(:last_month_route) do
      Route.create(starting_point: 'Zelazna 5', end_point: 'Popularna 40', date: Date.current.beginning_of_month)
    end

    let!(:today_route) do
      Route.create(starting_point: 'Pulawska 5', end_point: 'Lazurowa 40', date: Date.current)
    end

    it 'returns routes from current week' do
      weekly_routes = Route.weekly

      expect(weekly_routes.first).to eq(today_route)
      expect(weekly_routes.size).to eq(1)
    end

    it 'returns routes from current month' do
      monthly_routes = Route.monthly

      expect(monthly_routes.first).to eq(last_month_route)
      expect(monthly_routes.size).to eq(2)
    end
  end
end
