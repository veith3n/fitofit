#Fifofit

First install all dependencies with:
> bundle install

Then create database and seed it with random data:
> rake db:setup

Now you can start server with:
> rails s
