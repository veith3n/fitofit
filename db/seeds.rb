Route.destroy_all

streets = %w(Marszalkowska Slaska Pulawska Miodowa Twarda Obozna Poznanska Stawy)

20.times do
  starting_point = "#{streets.sample} #{rand(0..20)}, Warszawa"
  end_point = "#{streets.sample} #{rand(21..50)}, Warszawa"

  route = Route.new(starting_point: starting_point, end_point: end_point, distance: rand(1..30), date: rand(1..30).days.ago)
  route.save(validate: false)
  print '.'
end

10.times do
  starting_point = "#{streets.sample} #{rand(0..20)}, Warszawa"
  end_point = "#{streets.sample} #{rand(21..50)}, Warszawa"

  route = Route.new(starting_point: starting_point, end_point: end_point, distance: rand(1..30), date: Date.current)
  route.save(validate: false)
  print '.'
end

puts ' '
puts 'Routes generated'
