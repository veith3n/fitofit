class AddDateToRoute < ActiveRecord::Migration[5.1]
  def change
    add_column :routes, :date, :date
  end
end
