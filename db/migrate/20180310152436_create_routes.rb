class CreateRoutes < ActiveRecord::Migration[5.1]
  def change
    create_table :routes do |t|
      t.string :starting_point
      t.string :end_point
      t.decimal :distance, precision: 3, scale: 3
      t.date :date

      t.timestamps
    end
  end
end
