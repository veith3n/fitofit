class RemoveDateFromRoute < ActiveRecord::Migration[5.1]
  def change
    remove_column :routes, :date, :date
  end
end
