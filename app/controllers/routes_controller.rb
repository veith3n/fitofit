class RoutesController < ApplicationController

  def index
    @routes = Route.all
  end

  def show
    @route = Route.find(params[:id])
  end

  def new
    @route = Route.new
  end

  def create
    @route = Route.new(route_params)

    if @route.save
      redirect_to stats_path, notice: 'Created successfully'
    else
      render :new
    end
  end

  def stats
    grouped_routes = Route.monthly.group(:date)

    month_routes_distance_sum = grouped_routes.present? && grouped_routes.sum(:distance).map do |route|
      DailyStatsPresenter.new(route).to_hash
    end

    @routes = month_routes_distance_sum
    @weekly_stats = Route.weekly_stats
  end

  private

  def route_params
    params.require(:route).permit(:starting_point, :end_point)
  end
end
