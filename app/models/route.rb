class Route < ApplicationRecord
  validates_presence_of :starting_point, :end_point
  validate :start_cords_presence, on: :create, if: :starting_point?
  validate :end_cords_presence, on: :create, if: :end_point?

  after_validation :calculate_distance, :set_date

  scope :weekly, lambda {
    where(date: Date.current.beginning_of_week..Date.current.end_of_week).order(:date)
  }

  scope :monthly, lambda {
    where(date: Date.current.beginning_of_month..Date.current.end_of_month).order(:date)
  }

  scope :weekly_stats, -> { weekly.sum(:distance) }

  def start_cords_presence
    !start_cords && errors.add(:starting_point, 'is not correct')
  end

  def end_cords_presence
    !end_cords && errors.add(:end_point, 'is not correct')
  end

  def start_cords
    @start_cords ||= Geocoder.coordinates(starting_point)
  end

  def end_cords
    @end_cords ||= Geocoder.coordinates(end_point)
  end

  def calculate_distance
    self.distance = Geocoder::Calculations.distance_between(start_cords, end_cords)
  end

  def set_date
    self.date = Date.current
  end
end
