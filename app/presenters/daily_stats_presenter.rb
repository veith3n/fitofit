class DailyStatsPresenter
  def to_hash
    {
        month: daily_stats[0].strftime('%B'),
        date: daily_stats[0].present? && daily_stats[0].strftime('%d. %B'),
        distance: "#{daily_stats[1]} km"
    }
  end

  def initialize(daily_stats)
    @daily_stats = daily_stats
  end

  private

  attr_reader :daily_stats
end
